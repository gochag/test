//
//  ProfileController.swift
//  InstagramGg
//
//  Created by Tarlan Hekimzade on 29/09/2019.
//  Copyright © 2019 Tarlan Hekimzade. All rights reserved.
//

import UIKit
import SnapKit

class ProfileController: UIViewController {
    
    let arry = [1,2,3,4,5,6,7,8,9,0]
    
    //AppBar & Navigation
    let appBarView = UIView()
    let navigationView = UIView()
    let navigationTitle = UILabel()
    let navigationRightButton = UIButton()
    
    //content
    let contentView = UIView()
    
    //User Info
    let userInfoView = UIView()
    let userPhoto = UIImageView()
    
    let userPost = UILabel()
    let userPostCount = UILabel()
    
    let userFolower = UILabel()
    let userFolowerCount = UILabel()
    
    let userFolowing = UILabel()
    let userFolowingCount = UILabel()
    
    //Edit user data
    
    let editUserDataView = UIView()
    let userNameLabel = UILabel()
    let editUserDataButton = UIButton()
    
    //Collection Storis
    let savedStories = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        setupNavigationView()
        contentUserInfo()
        setupEditUserDataView()
        setupAddStories()
    }
    
    
    func setupNavigationView(){
        appBarView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        navigationView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        view.addSubview(appBarView)
        view.addSubview(navigationView)
        
        navigationTitle.textAlignment = .center
        navigationTitle.text = "Khakimzade"
        navigationTitle.textColor = .black
        
        let navIcon = UIImage(named: "ic_navigation")
        navigationRightButton.setImage(navIcon, for: .normal)
        
        navigationView.addSubview(navigationTitle)
        navigationView.addSubview(navigationRightButton)
        
        appBarView.snp.makeConstraints { make in
            make.left.top.right.equalToSuperview()
            make.height.equalTo(44)
        }
        
        navigationView.snp.makeConstraints { make in
            make.top.equalTo(appBarView.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(50)
        }
        
        navigationTitle.snp.makeConstraints { make in
            make.left.top.bottom.right.equalToSuperview()
        }
        
        navigationRightButton.snp.makeConstraints { make in
            make.top.right.bottom.equalToSuperview()
            make.width.equalTo(navigationRightButton.snp.height)
        }
    }
    
    func contentUserInfo(){
        contentView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        view.addSubview(contentView)
        
        userInfoView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        contentView.addSubview(userInfoView)
        
        userPhoto.image = UIImage(named: "ic_photo")
        
        userInfoView.addSubview(userPhoto)
        
        contentView.snp.makeConstraints { make in
            make.top.equalTo(navigationView.snp.bottom)
            make.left.bottom.right.equalToSuperview()
        }
        
        userInfoView.snp.makeConstraints { make in
            make.left.top.right.equalToSuperview()
            make.height.equalTo(120)
        }
        
        userPhoto.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(8)
            make.centerY.equalTo(userInfoView)
            make.height.equalTo(100)
            make.width.equalTo(userPhoto.snp.height)
            
            userPhoto.layer.borderWidth = 2
            userPhoto.layer.masksToBounds = false
            userPhoto.layer.borderColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
            userPhoto.layer.cornerRadius = 50
            userPhoto.clipsToBounds = true
        }
        
        let postStackView = UIStackView()
        postStackView.axis = .vertical
        postStackView.alignment = .center
        userPostCount.text = "0"
        userPostCount.textColor = .black
        userPost.text = "Post"
        userPost.textColor = .black
        postStackView.addArrangedSubview(userPostCount)
        postStackView.addArrangedSubview(userPost)
        
        let followerStackView = UIStackView()
        followerStackView.axis = .vertical
        followerStackView.alignment = .center
        userFolowerCount.text = "0"
        userFolowerCount.textColor = .black
        userFolower.text = "Follower"
        userFolower.textColor = .black
        followerStackView.addArrangedSubview(userFolowerCount)
        followerStackView.addArrangedSubview(userFolower)
        
        let followingStackView = UIStackView()
        followingStackView.axis = .vertical
        followingStackView.alignment = .center
        userFolowingCount.setText(text: "0", size: 16)
        userFolowingCount.textColor = .black
        userFolowing.setText(text: "Following", size: 14)
        userFolowing.textColor = .black
        followingStackView.addArrangedSubview(userFolowingCount)
        followingStackView.addArrangedSubview(userFolowing)
        
        let userActivityStackView = UIStackView(arrangedSubviews: [postStackView,followerStackView,followingStackView])
        userActivityStackView.axis = .horizontal
        userActivityStackView.distribution = .fillEqually
        userActivityStackView.spacing = 20
        userActivityStackView.translatesAutoresizingMaskIntoConstraints = false
        
        userInfoView.addSubview(userActivityStackView)
        userActivityStackView.snp.makeConstraints { make in
            make.centerY.equalTo(userInfoView)
            make.left.equalTo(userPhoto.snp.right)
            make.right.equalToSuperview().inset(8)
        }
    }
    
    func setupEditUserDataView(){
        editUserDataView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        contentView.addSubview(editUserDataView)
        
        userNameLabel.text = "Tarlan Hekimzade"
        userNameLabel.textColor = .black
        editUserDataView.addSubview(userNameLabel)
        
        editUserDataButton.setTitle("Edit profile", for: .normal)
        editUserDataButton.layer.borderWidth = 1
        editUserDataButton.layer.masksToBounds = false
        editUserDataButton.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        editUserDataButton.layer.cornerRadius = 4
        editUserDataButton.clipsToBounds = true
        editUserDataButton.setTitleColor(.black, for: .normal)
        editUserDataView.addSubview(editUserDataButton)

        
        editUserDataView.snp.makeConstraints { make in
            make.top.equalTo(userInfoView.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(80)
        }
        
        userNameLabel.snp.makeConstraints { make in
            make.left.top.right.equalToSuperview().inset(8)
        }
        
        editUserDataButton.snp.makeConstraints { make in
            make.top.equalTo(userNameLabel.snp.bottom).offset(8)
            make.left.right.equalToSuperview().inset(16)
        }
    }
    
    func setupAddStories(){
        savedStories.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.contentView.addSubview(savedStories)
        savedStories.delegate = self
        savedStories.dataSource = self
        savedStories.showsHorizontalScrollIndicator = false
        savedStories.register(ItemStoriesCell.self, forCellWithReuseIdentifier: "cellId")
        let layout = savedStories.collectionViewLayout as! UICollectionViewFlowLayout
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        

        savedStories.snp.makeConstraints { make in
            make.top.equalTo(editUserDataView.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(80)
        }
    }
}

extension ProfileController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! ItemStoriesCell
        cell.label.text = "\(arry[indexPath.row])"
        cell.label.textColor = .black
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 70.0, height: 70.0)
    }
}

class ItemStoriesCell :UICollectionViewCell{
    
    let view = UIView()
    let label = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        view.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        
        self.contentView.addSubview(view)
        view.snp.makeConstraints { mark in
            mark.edges.equalToSuperview()
            mark.height.width.equalTo(100)
            
            view.layer.borderWidth = 1
            view.layer.masksToBounds = false
            view.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            view.layer.cornerRadius = 35
            view.clipsToBounds = true
        }
        
        view.addSubview(label)
        
        label.snp.makeConstraints { make in
            make.center.equalTo(view.snp.center)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
