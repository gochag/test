//
//  TabBarCell.swift
//  InstagramGg
//
//  Created by Tarlan Hekimzade on 06/10/2019.
//  Copyright © 2019 Tarlan Hekimzade. All rights reserved.
//

import UIKit

class TabBarCell: UICollectionViewCell {
    
    static let ID = "TabBarCell"
    
    let label = UILabel()
    let line = UIView()
    
    override var isSelected: Bool{
        didSet{
            if(isSelected){
                self.line.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            }else{
                self.line.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        self.contentView.addSubview(label)
        label.snp.makeConstraints { make in
            make.center.equalTo(contentView.snp.center)
        }
        
        self.contentView.addSubview(line)
        self.line.snp.makeConstraints{
            $0.left.right.equalToSuperview()
            $0.top.equalTo(label.snp.bottom)
            $0.height.equalTo(2)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
