//
//  ContentMenuController.swift
//  InstagramGg
//
//  Created by Tarlan Hekimzade on 05/10/2019.
//  Copyright © 2019 Tarlan Hekimzade. All rights reserved.
//

import UIKit
import SnapKit

class ContentMenuController: UIViewController {

    let headerHeight = 220
    
    lazy var collectionView:UICollectionView = {
        let cv = UICollectionView(frame: self.view.frame, collectionViewLayout:UICollectionViewFlowLayout.init())
        cv.contentInset = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        cv.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        return cv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        self.view.addSubview(collectionView)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(MenuCell.self, forCellWithReuseIdentifier: MenuCell.cellId)

        collectionView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
    }
}

extension ContentMenuController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MenuCell.cellId, for: indexPath) as! MenuCell
        cell.label.text = "Position \(indexPath.row)"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (collectionView.frame.width/2) - 22, height: 200.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
           return 8
       }
}

class MenuCell: UICollectionViewCell {
    
    static let cellId = "MenuCell"
    let label = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.contentView.backgroundColor = .blue
        
        self.contentView.addSubview(label)
        label.snp.makeConstraints{
            $0.centerY.equalTo(contentView.snp.centerY)
            $0.left.right.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
