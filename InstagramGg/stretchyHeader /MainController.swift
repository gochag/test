//
//  MainController.swift
//  InstagramGg
//
//  Created by Tarlan Hekimzade on 05/10/2019.
//  Copyright © 2019 Tarlan Hekimzade. All rights reserved.
//

import UIKit
import SnapKit

class MainController: UIViewController {
    
    let titleLabel = UILabel()

    let arrayTabBar = ["Doner","Burger","Drink","Pizza","Salat","All"]
    
    //headerContent
    let imgHeader = UIImageView()
    
    lazy var titleHeader:UILabel = {
        let l = UILabel()
        return l
    }()
    lazy var cuisineHeader:UILabel  = {
        let l = UILabel()
               return l
    }()
    lazy var distanceHeader:UILabel = {
        let l = UILabel()
               return l
    }()
    lazy var descriptionHeader:UILabel = {
        let l = UILabel()
               return l
    }()
    
    let tabBarHeight = UIApplication.shared.statusBarFrame.height + 80
    let scrollHeaderInset:CGFloat = 500
    
    lazy var tabBarView:UIView = {
        let v = UIView()
        return v
    }()
    
    lazy var collectionView:UICollectionView = {
        let layout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        cv.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        cv.register(TabBarCell.self, forCellWithReuseIdentifier: TabBarCell.ID)
        cv.showsHorizontalScrollIndicator = false
        cv.backgroundColor = .white
        return cv
    }()
    
    lazy var headerView:UIView = {
        let v = UIView()
        v.backgroundColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
//        v.image = UIImage(named: "ic_photo")
        return v
    }()
    
    lazy var scrollView:UIScrollView = {
        let sv = UIScrollView()
        sv.backgroundColor = #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1)
        sv.delegate = self
        sv.contentInset = UIEdgeInsets(top: scrollHeaderInset, left: 0, bottom: 0, right: 0)
        return sv
    }()
    
    lazy var menuController:UIViewController  =  ContentMenuController()
    var headerLessThanTopConstraint: Constraint?
    var headerTopConstraint: Constraint?
    var tabBarConstraint:Constraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        
        setupTabBar()
        setupScrollView()
        setupHeader()
        setupContentView()
    }
    
    private func setupTabBar(){
        self.view.addSubview(tabBarView)
        tabBarView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        tabBarView.snp.makeConstraints{
            $0.left.right.equalToSuperview()
            self.tabBarConstraint = $0.top.equalToSuperview().inset(-tabBarHeight).constraint
            $0.height.equalTo(tabBarHeight)
        }
        
        tabBarView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints{
            $0.center.equalTo(tabBarView.snp.center)
        }
        titleLabel.text = "McDonald's"
        //
        tabBarView.addSubview(collectionView)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.snp.makeConstraints{
            $0.height.equalTo(40)
            $0.left.equalTo(tabBarView.snp.left)
            $0.right.equalTo(tabBarView.snp.right)
            $0.bottom.equalTo(tabBarView.snp.bottom)
        }
    }
    
    private func setupScrollView(){
        self.view.addSubview(scrollView)
        self.scrollView.snp.makeConstraints{
            $0.left.right.bottom.equalToSuperview()
            $0.top.equalTo(tabBarView.snp.bottom)
        }
    }
    
    private func setupHeader(){
        scrollView.addSubview(headerView)
        headerView.snp.makeConstraints{
            $0.leading.trailing.equalTo(scrollView)
            self.headerTopConstraint = $0.top.equalTo(view.snp.top).priority(999).constraint
            self.headerLessThanTopConstraint = $0.top.lessThanOrEqualTo(view.snp.top).constraint
        }
        
        
        imgHeader.image = UIImage(named: "food")
        imgHeader.contentMode = .scaleAspectFill
        headerView.addSubview(imgHeader)
        
        imgHeader.snp.makeConstraints{
            $0.left.top.right.equalTo(headerView)
            $0.width.equalTo(headerView.snp.width)
        }
        
        let headerContentView = UIView()
        headerContentView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        headerView.addSubview(headerContentView)
        
        headerContentView.snp.makeConstraints{
            $0.top.equalTo(imgHeader.snp.bottom)
            $0.height.equalTo(150)
            $0.left.right.bottom.equalTo(headerView)
        }
        
        headerContentView.addSubview(titleHeader)
        headerContentView.addSubview(distanceHeader)
        headerContentView.addSubview(cuisineHeader)
        headerContentView.addSubview(descriptionHeader)
        
        titleHeader.snp.makeConstraints{
            $0.left.right.top.equalTo(headerContentView).offset(16)
            titleHeader.text = "McDonald's"
            titleHeader.font = UIFont(name:"HelveticaNeue-Bold", size: 22.0)
        }
        
        cuisineHeader.snp.makeConstraints{
            $0.top.equalTo(titleHeader.snp.bottom).offset(4)
            $0.left.right.equalTo(headerContentView).offset(16)
            cuisineHeader.setText(text: "Russian, Azerbaijan", size: 16)
            cuisineHeader.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        }
        
        distanceHeader.snp.makeConstraints{
            $0.top.equalTo(cuisineHeader.snp.bottom).offset(4)
            $0.left.right.equalTo(headerContentView).offset(16)
            distanceHeader.setText(text:  "Distance 4.1 km / 15 min", size: 16)
            distanceHeader.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        }
        
        descriptionHeader.snp.makeConstraints{
            $0.top.equalTo(distanceHeader.snp.bottom).offset(4).priority(999)
            $0.left.right.equalTo(headerContentView).offset(16)
            descriptionHeader.setText(text: "You might have seen a collapsable or stretchable tableview header in android. If you check the whatsapp profile/ group settings page , you can see this", size: 16)
            descriptionHeader.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            descriptionHeader.numberOfLines = 0
        }
    }
    
    private func setupContentView(){
        self.addChild(menuController)
        scrollView.addSubview(menuController.view)
        menuController.view.snp.makeConstraints{
            $0.top.equalTo(headerView.snp.bottom)
            $0.top.equalTo(scrollView)
            $0.left.right.bottom.equalTo(scrollView)
            $0.width.equalTo(scrollView)
            $0.height.equalToSuperview()
        }
    }
    
}

extension MainController :UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        guard let headerTopConstraint = headerTopConstraint, let headerLessThanTopConstraint = headerLessThanTopConstraint, let tabBarConstraint = tabBarConstraint else {
            return
        }
        
        let y = scrollView.contentOffset.y
        let offset = y > 0 ? -y : 0
        print(y)
        headerLessThanTopConstraint.update(offset: offset)
        headerTopConstraint.update(offset: offset)
        
        
        if(y > -scrollHeaderInset){
            let x = -(y * 100 / scrollHeaderInset)
            let k = -(x * tabBarHeight / 100)
            
            if x > 90{
                tabBarConstraint.update(offset: -tabBarHeight)
            }else{
                tabBarConstraint.update(offset: k)
            }
        }
    }
}

extension MainController :UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayTabBar.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TabBarCell.ID, for: indexPath) as! TabBarCell
        cell.label.text = arrayTabBar[indexPath.row]
        cell.label.textColor = .black
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 32
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
    }
}
