//
//  ExtLablel.swift
//  InstagramGg
//
//  Created by Tarlan Hekimzade on 29/09/2019.
//  Copyright © 2019 Tarlan Hekimzade. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    public func setText(text:String, size:CGFloat,_ aligment:NSTextAlignment = .left){
        self.textAlignment = aligment
        self.text = text
        self.font = UIFont(name: "Avenir-Light", size: size)
    }
}
