//
//  ContentView.swift
//  InstagramGg
//
//  Created by Tarlan Hekimzade on 28/09/2019.
//  Copyright © 2019 Tarlan Hekimzade. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello World")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
